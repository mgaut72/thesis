\section{Introduction}

Computational algebra is a well established field.  With respect to
group theory, the field has been concerned with being able to efficiently
represent groups and perform computations with a group acting on a set.\\

There exist a few popular libraries and programming languages for performing
group theoretic calculations.  The most widely used of these computational
frameworks, GAP \cite{GAP} is written in C.
C is a programming language that is typically chosen for performance-sensitive
applications.
In the programming languages
world, C is described as imperative, statically typed, and low-level.
Being imperative, C is a language that describes a program's behavior using
statements that change the state of execution.  The static typing of C means
that variables are declared to be of a certain type, and should behave only
like the type they are declared to be.  Similarly, functions are declared to
have arguments and a return value of particular types, and can only be called
when the arguments have the proper type (or can be automatically converted).
C is described as low-level due to the fact that the core of the language
was designed to be very close to the assembly code it compiles to.
The fact that the translation from C to machine code is relatively simple
allows for compiler optimizations that make it a fast language.
However, this comes at a cost.
More complex data structures are not built into the language, and
must be programmed by hand or taken from an open source library.\\

The static type system in C does provide some guarantees about the behavior
of programs, functions, and types.  However, C makes some sacrifices in the
strictness of its static type system in order to be a language that closely
corresponds to machine code.\\

On the other end of the spectrum, we have Haskell.  Haskell is a purely
functional programming language with strict static typing and lazy evaluation.
A purely functional language describes a program's behavior in terms of
expressions and pure functions.  This type of program structure is similar to
mathematical functions; you get the same output for the same input every time.
Interacting with data outside the function becomes more complicated, but is
still possible.  The strict static typing means that conversion between
types when working with variables must be explicit, and defined by a pure
function.  A pure function is a function whose return value is determined only by
its input values (not any other system state),
and has no side-effects on the system state.
With this complexity, Haskell brings the ability to make
guarantees about the behavior of functions and programs that C simply cannot
make.  Finally, lazy evaluation is the idea that a computation is not performed
until the results are needed.  This means that languages like Haskell have
the ability to efficiently work with very large or even infinite data
structures, so long as the program does not attempt to force the evaluation
of these structures.\\

This paper seeks to explore and exploit the properties of the programming
language Haskell for more intuitive implementations and interfaces for
group theoretic computations.  Haskell is already regarded as an `algebraic'
programming language, given its emphasis on types, structure, and operations
on these types.\\
