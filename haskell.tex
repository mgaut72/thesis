\section{Haskell Implementations}

Haskell is a lazy-evaluated, purely functional, strictly typed language.
Lazy evaluation means that computations are not performed until the results
are needed.  A function call in Haskell does not compute a result.  Rather,
when calling a function in Haskell, the language gives you a promise to give
you the result when you want to actually use it.  Being purely functional,
state is not a natural thing in Haskell, though there are methods of simulating
state that are outside the scope of this brief explanation of the language.
Rather, the language is centered on more mathematical-style expressions and
values over statements with side effects dependent on state.
Finally, being strictly typed, Haskell allows us to build type constraints
into our programs, which will be enforced at compile-time.  These properties
of the language allow us to express relationships between data types that
cannot readily be done in other languages.  For an introduction to Haskell,
LYAH \cite{LYAH} is recommended.

The following section will serve to implement the Orbit and Stabilizer
algorithms using idiomatic Haskell code.
Full code for this project can be found on the author's github \cite{github}.

\subsection{Representing a Group}

The type system of Haskell does not have a simple
way of enforcing that the binary operation of a group be
associative, so this is a responsibility that is put on the programmer.
We can, however, define a Haskell typeclass that captures the other
properties of a group.

A Haskell typeclass is similar to an interface in an object-oriented
language such as Java or C++.  We define the name of the class,
and then the functions that the class must support.

Consider the following Haskell code snippet, which defines a typeclass for
groups:

\begin{verbatim}
class Group a where
  identity :: a
  inverse  :: a -> a
  times    :: a -> a -> a
\end{verbatim}

The means we have a class called \texttt{Group} which consists of values
of type \texttt{a}.  Next, we have three functions that are required to exist
for this class.  The first is something called \texttt{identity}.  We read that
line as ``identity has type a''.  This means for things of type \texttt{a} to be
a group, they must have a value that serves as the identity. The next
requirement is a function called \texttt{inverse}.  We read the line as
``inverse has type a to a''.  This means that \texttt{inverse} is a function
that takes something of type \texttt{a} and results in something of
type \texttt{a}.  In standard mathematical notation, we would write
$ inverse : a \to a$.  Meaning that \texttt{inverse} is a function whose domain and codomain
the set of all things of type $a$.  The final requirement for the \texttt{Group}
class is something called \texttt{times}.  Like \texttt{inverse}, \texttt{times}
is a function.  But \texttt{times} is a binary function, in fact it is the
binary function that completes the definition of our group.  The mathematical
notation for \texttt{times} is $times : a \times a \to a$; It takes two elements, each
from the set of all things of type $a$, and results in something of type $a$.
\\

Note that there are some shortcomings to the properties that this typeclass can
enforce.  The typeclass states that there must be an element called \texttt{identity}.
It makes no claims on what identity is, meaning that it cannot force that
\texttt{identity `times` x == x} for all \texttt{x}.  Similarly, it requires that there
be a function called \texttt{inverse}, but it does not require
\texttt{(inverse x) `times` x == x `times` (inverse x) == identity}.  Furthermore,
a binary operation with the appropriate type signature is required, but we have
no guarantee that the operation is associative, as required by the definition of
a group.

We simply require that one who is implementing the \texttt{Group} typeclass check these
few properties.

\subsection{Representing a Group Acting on a Set}

Having established a \texttt{Group} typeclass, we can move on to a representation of Groups
acting on Sets.  Recall that for a group to act on a set, we must have a function
that combines a element from the group with the element from the set, producing a new element
from the set.  This function must be compatible with the group action, as well as the group
identity element (see the definition of group action given previously).

In Haskell, a group acting on a set can be represented in the following manner:

\begin{verbatim}
class Group a => GroupAction a b where
  action :: a -> b -> b
\end{verbatim}

The first line states that a \texttt{GroupAction} consists of elements of type \texttt{a}
and \texttt{b}.  It requires that the type \texttt{a} fulfills the \texttt{Group}
class.  The second line requires the existence of a function \texttt{action} which takes
a group element (something of type \texttt{a}) and a set element (something of type \texttt{b}),
and results in a set element. Again, we run into the minor issue of not being able to express the
compatibility and identity constraints, and those must be checked by the programmer.\\

The typeclass construct in Haskell allows us to easily define a relationship between two other
types or typeclasses.  We are not explicitly creating a \texttt{GroupAction} object.  Instead, we
say that something behaves like a group action if it fulfills the rules given above.  We will now
delve into an implementation of the \texttt{Group} typeclass, and then use it to fulfill the
\texttt{GroupAction} typeclass.


\subsection{Permutation Groups in Haskell}

Recall from previous sections that permutation groups have a natural group action on the
elements they permute.  Let us then implement permutation groups in Haskell, keeping in mind
our goals of fulfilling the \texttt{Group} and \texttt{GroupAction} typeclasses.\\

\subsubsection{Underlying Structure of Permutations}

A common implementation of permutations in many programming languages is storing them
in some sort of key-value map.  The keys and values would have the same type (the type
of the elements that are being permuted).  This is a popular method of implementing
permutations due to the fact that the performance cost of looking up an element
in the map is constant.  However, the constant time lookup that is used in the traditional
Hash-map is not a purely functional data structure due to statefulness of
keeping the data structure as a simple table and modifying memory locations based on the
hash value of a key; thus a hash-map is not idiomatic Haskell.  Instead,
the \texttt{Map} type in Haskell is actually implemented as a balanced binary tree, where
elements are inserted into the tree based on their keys.  Thus, the lookup of an element
in the \texttt{Map} type are (in the average case) logarithmic with the number of elements
in the map, rather than
constant.  The fact that the \texttt{Map} is a balanced tree means that the worst case
of lookup being linear with the number of elements in the map is avoided.  One downside
this implementation of \texttt{Map} is that there needs to be a relative ordering
of the contents.  This necessary is so there can be a sorted insertion into the underlying
binary tree.\\

Thus we will implement a permutation as a map.  In Haskell code, we create the new
type alias for a permutation as follows:

\begin{verbatim}
newtype Permutation a = P (M.Map a a) deriving (Ord)
\end{verbatim}

This simply means that a permutation of elements of type \texttt{a} will be represented as a map from
values of type \texttt{a} to values of type \texttt{a}.  This map will be wrapped in something called \texttt{P}.
The extra `\texttt{deriving (Ord)}' means
that we want Haskell to automatically find a way to sort permutation elements.

\subsubsection{Operations on Permutations}

To fulfill the \texttt{Group} typeclass, we first need to determine a way to create an
identity permutation.  We will simply let this be the empty map, and establish the convention that,
when we go to look up an element that is not in the map, we assume it has been mapped to itself.
Thus, the empty map serves as the identity element for this permutation implementation.

Therefore, our lookup function needs to be implemented as follows:
\begin{verbatim}
lookup :: (Ord a) => a -> Permutation a -> a
lookup a (P p) = M.findWithDefault a a p
\end{verbatim}

The means we check to see if the value we want is a key in the \texttt{Map}.  If not
we default to returning the requested item, otherwise we perform the lookup
as normal.

In Haskell, this gives:

\begin{verbatim}
zero :: (Ord a) => Permutation a
zero = P M.empty
\end{verbatim}
\\

Next, we need to establish a way of computing the inverse of the permutation.
Haskell provides a way of flattening a \texttt{Map} into a list of 2-tuples,
where the first element is the key, and the second element is the value. There
is a corresponding function which creates a \texttt{Map} given a list of tuples
in the same form.
Given a \texttt{Map} which represents our permutation, we will flatten it,
swap all of the values in the tuple, and then re-build the map.

In Haskell, this gives:

\begin{verbatim}
negate :: (Ord a) => Permutation a -> Permutation a
negate (P m) = P . M.fromList . Prelude.map swap . M.toList $ m
\end{verbatim}
\\

Now becomes a good time to delve somewhat into Haskell syntax.
The dot function ``.'' is the Haskell way of denoting function composition.
the dollar function ``\$'' is the Haskell way of denoting function application.

Let us break the definition of \texttt{negate} down piece by piece, for clarity.

The first line is called the \emph{type signature}.  This tells us the arguments and return types
of the function.  Negate is a function that take a \texttt{Permutation a} and returns a \texttt{Permutation a},
but required that \texttt{a} be a member of the \texttt{Ord} typeclass.  This means that we must have
a way to sort elements of type \texttt{a} (which is necessary for the underlying map implementation.

The next line is the function definition.  The first word is the function name, and everything
up until the equals sign are arguments (\texttt{negate} has one, which we use \emph{pattern matching}
on so that we have access to the underlying map, which we call \texttt{m}).

Finally we have the function body.  Looking to the end of the line (\texttt{\$ m}), we see that we are applying
some function to the map that represents our permutation.  That function is found to the right of the
dollar sign.  As with all function composition, we start at the inner-most function, and work our way out.
First, we flatten the map into a list of tuples, using \texttt{M.toList}.  Then, we take the standard library function
called \texttt{swap} and map (as in ``apply to all elements in the list'') it, reconstruct
the \texttt{Map}, and replace the map back in our wrapper type \texttt{P}.\\

Now the last requirement to fulfill the \texttt{Group} typeclass is the binary operator.  We need a way to
combine permutations through function composition.  For every key in the first map (representing the first
permutation), we find the value, which we use as the key in the second map, and perform the lookup.  Since
we are defaulting to no key in the map means the permutation fixes that element, there are some elements that
may be in the second map which are not in the first map.  Thus we need to add those values in.
For example, suppose we have the composition of the two permutations (written in cycle notation):
$(1 2 3 4 5)(1 2 3) = (1 3 2 4 5)$.  If we just consider the first (right-most) permutation, we miss out on
keys $4$ and $5$.  Thus we perform all the lookups in the first permutation, then add in the mappings
that were not covered.

The Haksell implementation is as follows:

\begin{verbatim}
compose :: (Ord a) => Permutation a -> Permutation a -> Permutation a
pa@(P a) `compose` pb@(P b) = P $ M.union x a
 where x = M.map (flip lookup pa) b
\end{verbatim}

First, we construct the intermediate \texttt{Map} called \texttt{x}.  For every value in the \texttt{b} permutation,
we treat it as the key in the \texttt{a} permutation, and perform a lookup.  The original value of the \texttt{b}
permutation is then replaced with the result of the lookup.

Then we take the union of \texttt{x} with the \texttt{a} permutation.  The implementation of union favors
the first argument.  This means if the key already exists in the \texttt{x} permutation, we do not overwrite it.
If the key does not exist in the \texttt{x} permutation, we use the key-value pair from the \texttt{a} permutation.\\

We can now say that \texttt{Permutation a} is an instance of the \texttt{Group} class like so:

\begin{verbatim}
instance Ord a => Group (Permutation a) where
  identity = zero
  inverse  = negate
  times    = compose
\end{verbatim}
\\

We previously implemented the \texttt{lookup} function for use in the \texttt{compose}
function.  One should observe that the action of looking up a key in a permutation is
exactly equivalent to computing the group action of the permutation on that key.

Thus, we can fulfill the \texttt{GroupAction} class in the following way:

\begin{verbatim}
instance Ord a => GroupAction (P.Permutation a) a where
  action = flip lookup
\end{verbatim}

Because of the order of the arguments in \texttt{lookup} is backwards when compared to the
ordering required in the \texttt{GroupAction} class, we use the \texttt{flip}
function to flip the arguments of \texttt{lookup}, and we have fulfilled the
\texttt{GroupAction} typeclass.

\subsection{Orbit Algorithm in Haskell}

Haskell is a very recursive language in nature, so we will adapt the orbit algorithm into
a recursive method.  The recursive structure of the orbit algorithm is suggested by the fact
that we are building up the orbit by using the same subsection of the given algorithm.  We will
implement the naive orbit algorithm, which can easily be extended to the optimized orbit algorithm.

Since lists are a natural and default data structure built into Haskell, we use lists in the
computation of the orbit, then wrap the result into a set once we are done.  The actual
computation step will be called \texttt{orbit'}, and the wrapper will be called \texttt{orbit}.

The implementation is as follows:

\begin{verbatim}
orbit generators w = S.fromList $ orbit' (S.toList generators) [w]

orbit' gens orb
  | S.fromList new == S.fromList orb = orb
  | otherwise  = orbit' gens $ nub (orb ++ new)
 where new = nub [action g x | g <- gens, x <- orb]
\end{verbatim}

Most of the work in the algorithm is being done with the calculation of the list \texttt{new}.
The list comprehension (much like set-builder notation), runs over all elements of \texttt{gens}
and all elements of \texttt{orb}, and stores all possible group actions in a list.
\texttt{nub} is simply a function that removed duplicates from our list.  The calculation
of \texttt{new} directly corresponds to the calculation done in the \texttt{for all} loops of
the orbit algorithm.

After we have computed all group actions, we need to check if we found any new elements in the orbit.
This is done by putting \texttt{orb} and \texttt{new} into sets, and checking them for equality.
In the event that they are equal, we know that there is nothing new to add to the orbit, and
then we are done.  If \texttt{new} contained new elements, we recursively call our function, with
the same generators, and the combination of the existing orbit (\texttt{orb}) and the new
elements (\texttt{new}).

\subsection{Schreier Trees in Haskell}

To implement a Schreier Tree in Haskell, we will use an existing graph library.
Due to the functional nature of Haskell, the graph implementing has a few quirks that
necessitate explaining.

The nodes must have type \texttt{Int}.  This means
we need a way to convert elements of the set the group is acting on to an \texttt{Int} type.
For the case of permutations, the set we are acting on is already \texttt{Int}, so we need
not worry, but to be generic, we use a Haskell typeclass called \texttt{Enum}, which
dictates that elements of the typeclass can be converted to and from integers, using
the functions \texttt{toEnum} and \texttt{fromEnum} respectively.
However, we then have the option to use a labeled node, which means the graph will
store the nodes as a tuple of \texttt{(Int, a)}, where \texttt{a} can be anything.
This allows us to store the original element of the set we are acting on, which will
be useful for computational purposes later on.\\

We now investigate how to perform the insertions and operations as outline by the previous section on
the algorithm.

Recall that we loop over each new vertex, applying each generator to that new vertex, and
then seeing if we can add it to the tree without breaking the required properties of the
Schreier Tree.

The general approach is a follows, outlined in a bottom-up fashion.\\

The most primitive operation we need to perform is adding a new node to the tree,
and drawing an edge with the node/generator combination that produce the new node.

This \texttt{addNode} function will be the basis for the \texttt{schreierTree} function
that is yet to come.

\begin{verbatim}
addNode n (ns,t) g
  | (fromEnum n') `G.gelem` t = (ns, t)
  | otherwise                 = (ns ++ [n'], newT)
 where n' = action g n
       nd newNode = (fromEnum newNode, newNode)
       newT = G.insEdge (fromEnum n', fromEnum n, g)
            . G.insEdge (fromEnum n, fromEnum n', g)
            . G.insNode (nd n')
            $ t
\end{verbatim}

The function \texttt{addNode} takes the following input:
\begin{enumerate}
    \item
        \texttt{n} is the node which we are looking to apply a generator to in order to find a new node
    \item
        \texttt{(ns,t)} is a tuple, where \texttt{ns} is a list of all the nodes we have added to the tree.
        We need to keep track of \texttt{ns} because this is the terminating condition for the Schreier Tree
        algorithm.  $t$ is the current tree we are working with.

    \item
        \texttt{g} is the generator which we will apply to \texttt{n} in attempts of finding a new node to add
        to the tree without creating any cycles.
\end{enumerate}

We perform the computation of the group action of \texttt{g} on \texttt{n}, which we store in \texttt{n'}.
We then check to see whether \texttt{n'} is an element of the tree already.  In which case we return the
original \texttt{(ns,t)} tuple, since we have not modified the tree in any way.

However, if \texttt{n'} is not an element of the tree, we need to add \texttt{n'} as a node, and add an
edge connecting \texttt{n} and \texttt{n'}.  Since the graph library works with directed graphs, we add
an edge in each direction between the two nodes.  \texttt{newT} is the application of the three insertions
(two for edges and one for the node) to the existing tree \texttt{t}.

We then return \texttt{(ns ++ [n'], newT)}, which adds \texttt{n'} to the list of new nodes, also gives us the
updated tree.\\

Now that we have the most basic step in the Schreier Tree algorithm, we need to perform the iterations
as outlined in the algorithm.  Recall that the Schreier Tree algorithm consisted of two nested \texttt{for all}
loops, with the inner loop running over all generators, and the outer loop running over all vertices that don't
have any children.

To simulate the inner loop, we will successively call the \texttt{addNode} function for each generator
that make up our group.  Since \texttt{addNode} will either do nothing or update the tree and new element
set, we can elegantly represent the inner for loop of the algorithm as a fold.  A fold
takes a function, a starting value, and a list.  The fold then takes one element from the list, uses
the function in conjunction with the starting value to make up updated value, whose type is the same of
the starting value.  It then repeats this process with the remainder of the list, and the updated
value serving as the new starting value.

The function is as follows:

\begin{verbatim}
applyGens gens newAndTree n = S.foldl (addNode n) newAndTree gens
\end{verbatim}

The inputs to the \texttt{applyGens} function are the following:
\begin{enumerate}
    \item
        \texttt{gens} are the list of all generators of the group
    \item
        \texttt{newAndTree} is a tuple, with the first element being the list of recently added nodes,
        and the second value being the tree we are building up.
    \item
        \texttt{n} is the node we are currently focused on working our way out from.
\end{enumerate}

We simply use the \texttt{addNode n} function to fold over all the generators.  This will start
us out with the existing \texttt{newAndTree}, and by the end of the fold, it will be the result
of attempting to add new nodes $g.n$ for every generator $g$.
\\

Finally, we need to simulate the outer for-loop of the Schreier Tree algorithm, which is
and iteration over all nodes in the tree that don't have children.

Again, we will elegantly express this as a fold.  We will take the \texttt{applyGens} function
and use it with every new node in the tree.  This is effectively folding the \texttt{applyGens}
function over the list of new nodes, with the accumulator value being the tuple of
\emph{newer} nodes and the updated tree.

\begin{verbatim}
tree _ [] t = t
tree gens as t = tree gens as' t'
 where (as', t') = foldl (applyGens gens) ([],t) as
\end{verbatim}

The input to the \texttt{tree} function are as follows:
\begin{enumerate}
    \item
        \texttt{gens} are the generators for the group
    \item
        \texttt{as} are the new nodes (nodes without children that still may produce children)
    \item
        \texttt{t} is the current tree which we are building up.
\end{enumerate}

\texttt{tree} pattern matches on the case where \texttt{as} is the empty list.  That means that there
were no vertices added to the tree in the last step, and thus we have reached the terminating condition
for the algorithm, and can simply return \texttt{t}.

The remaining case is when there was at least one new vertex added to the tree in the last step.  We need
to then fold the \texttt{applyGens} function over the list of new nodes, attempting to apply all generators
and add new nodes and edges to the tree.  We seed the fold with the tuple \texttt{([], t)}.  The \texttt{[]}
represents the empty list, which means we are starting out with no new nodes having been added to the tree.
The fold will product \texttt{(as', t')}, which is the new node list and new tree after this iteration
of the algorithm.  We recursively call the \texttt{tree} function until we reach the base case.
\\

Finally, we need a wrapper function that simply takes the generators and the set element for which we
want the Schreier Tree rooted.  This wrapper function simply makes a tree containing only the root node,
and calls \texttt{tree} with the root node being the only element in the \emph{new node} list.

\begin{verbatim}
schreierTree :: (Enum a, GroupAction g a) => S.Set g -> a -> Gr a g
schreierTree gens a = tree gens [a] initialTree
 where initialTree = G.insNode (fromEnum a, a) G.empty
\end{verbatim}

We now see that we have as \texttt{schreierTree} function with the desired type signature.
Given that elements of type \texttt{g} are a group acting on type \texttt{a}, we have a set of
\texttt{g}-type elements (the generators), and an \texttt{a}-type element (the root for the tree),
and produce a graph with nodes of type \texttt{a} and edges of type \texttt{g}, \texttt{Gr a g}.

\subsection{Stabilizer Calculation in Haskell}

As previously suggested, the stabilizer calculation becomes trivial given a Schreier Tree rooted at
at the desired element.  The existence of Schreier's Lemma, which we explained, though did not
formally prove, allows us to traverse the Schreier Tree in order to compute the stabilizer.

Recall that the general idea of the stabilizer algorithm is that we loop over all generators
of the group, and then loop over all elements in the orbit of the element for which we are
calculating the stabilizer.  We check to see if an edge exists between the element of the orbit
and the group action of the current generator on the given orbit element.  If not, we add
the associated Schreier Element to the stabilizer set.

We will outline a few helper functions, then use them to compute the stabilizer.\\

First, we will develop a helper function that determines if an edge exists between two nodes.
This is the condition on whether or not we add elements to the stabilizer, and thus
a very important function.

In the graph library we are using, an edge is stored as a tuple of \texttt{(Int, Int, e)}
where the two \texttt{Int} elements are the IDs of the nodes, and \texttt{e} is the edge label.
For our purposes, the edge label type \texttt{e} is an element of the \texttt{Group} typeclass,
since the edges of the Schreier Tree are the generators that move from one vertex to the next.

Recall that all edges are directed, so when we are checking for an edge, we are really checking for
either one of the directed edges.

Finally, the \texttt{isEdge} function:

\begin{verbatim}
isEdge t i b ib = isJust $ find match es
 where es = G.labEdges t
       match (a,c,perm) = perm == b && ((e i == a && e ib == c) || (e ib == a && e i == c))
       e n = fromEnum n
\end{verbatim}

\texttt{isEdge} takes the following arguments:

\begin{enumerate}
    \item
        \texttt{t}: the tree we are working with
    \item
        \texttt{i, ib}: the two elements we are checking the edge between
    \item
        \texttt{b}: the group element we expect to be the edge between \texttt{i} and \texttt{ib}
\end{enumerate}

First, we get \texttt{es} the list of all edges in the graph (stores as tuples).

We have a helper function called \texttt{e} which converts an element from the set of the group
action into the \texttt{Int} identifier of its associated node.

Then, we define a function called \texttt{match}, which dictates whether for not a given
edge in the \texttt{(Int, Int, g)} tuple form matches the parameters to the \texttt{isEdge}
function. This is done by ensuring that the generator is the same as the expected, and that
the endpoints are the same as the requested endpoints (in either direction).

Finally, we use the built-in \texttt{find} function which runs \texttt{match} across
\texttt{es}.  If there is a match, the result of \texttt{find} will be \texttt{Just (node, node, edge)},
and if there is no match, the result of \texttt{find} will be \texttt{Nothing}.  We then use the
built-in function \texttt{isJust} to determine whether we found a match and return a boolean
indicator of whether the requested edge in in the graph.\\

The next helper function we need is related to the computation of the Schreier Element.
If we have not found an edge between two nodes, this means the associated Schreier Element
is a member of the stabilizer.

Recall that a Schreier Element is something of the form
$t_ibt_{i^b}^{-1}$, where $i$ is a member of the orbit, $b$ is a generator, $i^b$ is also
a member of the orbit, where $i^b = b.i$ the group action of $b$ on $i$. Finally, $t_i$ is
the path from the root of the Schreier Tree to the node $i$, and $t_{i^b}^{-1}$ is the element
that takes $i^b$ to the root of the tree.

For the purpose of doing programatic computations, it is important to note that the Schreier Element
expression given above is in permutation-composition form, rather than the generic group element
multiplication form.  That is, a Schreier Element is actually $t_i \circ b \circ t_{i^b}^{-1}$
which, when considered multiplicatively is actually $t_{i^b}^{-1} \times b \times t_i$ for the purpose
of this project.

Thus, it is necessary to be able to compute the group element that corresponds to the directed
path between two nodes in a Schreier Tree.

\begin{verbatim}
path t start end = foldl times identity . reverse . map snd . tail $ ns
 where G.LP ns = G.lesp (fromEnum start) (fromEnum end) t
\end{verbatim}

\texttt{path} takes as input the tree \texttt{t}, the start vertex \texttt{start} and the end vertex
\texttt{end}.  Given the construction of the Schreier tree, we know that we will only be calling \texttt{path}
on endpoints that are known to have a path between them.

The \texttt{path} function relies heavily on the path-finding function built into the graph library
we are using.  \texttt{G.lesp} is a built-in function that takes the start and end nodes, and the tree,
and gives us a list of the edges in the path.  The list elements are in the form \texttt{(Int, g)} where
the \texttt{Int} is the node label of the next node in the path, and the \texttt{g} is the edge label (in
our case the group generator) that got us from the previous node to this one.  Finally, we compose
a few functions that get all relevant permutations from this list and multiply them all together.

The output of \texttt{G.lesp} is such that the first element of the list contains \texttt{start} as the node,
but there is no logical edge.  Thus, the first function in the composition is \texttt{tail}, which keeps
everything but the first element of a list.  Next, we seek to pull the group element out of the
\texttt{(Int, g)} tuple, which is accomplished by using the \texttt{snd} function.  \texttt{snd} takes
the second element from a 2-tuple, and \texttt{map} applies the function to every element in the list.
The next function in our composition is \texttt{reverse}.  We must reverse the list due to the fact that group
element multiplication is not necessarily commutative (for permutations, it is not).  This reversal
works in tandem with the order of the fold in the next step.  We combine the list of permutations down
using the \texttt{times} function of the \texttt{Group} typeclass.  The direction of the fold (\texttt{foldl})
is what required the reversal of the list to preserve the proper order of the multiplication.

The result of the fold is a single group element, which can be used to compute a Schreier Element to add
to the stabilizer.
\\

Now all that is left is the implementation of the stabilizer calculation.

We will form the list of all orbit elements crossed with all group generators, and the calculated
group action.  Then for each $(x, g, g.x)$, we will see if it is an edge in the Schreier Tree.
If it is not, we compute the associated Schreier Element, and add it to the stabilizer we are building up.

\begin{verbatim}
stabilizer gens a = foldl checkAdd S.empty l
 where l = [ (i, b, action b i) | b <- S.toList gens, i <- S.toList (orbit gens a)]
       checkAdd s (i,b,ib) = if isEdge t i b ib
                               then s
                               else S.insert (e i b ib) s
       e i b ib = inverse (path t a ib) `times` b `times` path t a i
       t = schreierTree gens a
\end{verbatim}
\newpage

Like most of the Haskell algorithm implementations, we seek to elegantly express this calculation as a fold
over elements of interest. We create the list \texttt{l}, which contains a 3-tuple of
(orbit element, group element, group action on orbit element).  We when start with the empty set as the
accumulator value of the fold, and use the function \texttt{checkAdd} as the folding function.

\texttt{checkAdd} takes the current set of stabilizer element, and the current 3-tuple of interest.
As the stabilizer algorithm suggests, we check so see if there is an edge between the two given
nodes.  Not, we compute the Schreier Element using the function \texttt{e}, and add it to the set of stabilizers.
Otherwise, we simply propagate the existing set of stabilizers.

The Schreier element calculator \texttt{e} takes the two nodes, uses the \texttt{path} function to find
$t_i$ and $t_{i^b}^{-1}$ and then calculates $t_{i^b}^{-1} \times b \times t_i$.

Once \texttt{checkAdd} has been folded over all of the 3-tuples, it will have built up the stabilizer
by forming Schreier elements, and we are done.
