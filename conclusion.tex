\section{Conclusion}

Being a Turing complete programming language, Haskell is certainly capable of performing
the computations necessary to be used in a computational group theory setting.  The language
readily provides high-level constructs that make certain aspects of the representation and
computations very intuitive and friendly.  However, there are inherent drawbacks in being
a strictly typed pure functional language, that can sometimes leave the user wanting
more (or less) out of their language.\\

People who love Haskell love it for the guarantees the type system can make.  Being
able to catch a greater set of errors at compile-time, rather than run-time, has its
software engineering benefits.  It is easier and more comfortable to a developer
(especially on projects that are larger with respect to either their code-base, timeline,
or both), to be able to go into the code, make changes, and get immediate and useful feedback
from the compiler.  The pure functions and strict type-system can prevent programmers from
making invalid assumptions by catching these errors at the compile phase of a program.

When looking at the algorithms that have been developed, we are able to add constraints
and genericness through the \texttt{Group} and \texttt{GroupAction} that may not be as
elegantly expressed in other languages.  Take for example the type signature of the
orbit calculation:
\begin{verbatim}
orbit :: (GroupAction g a, Ord a) => Set g -> a -> Set a
\end{verbatim}

The compiler mandates that a set of elements of type \texttt{g} and an element of type
\texttt{a} are arguments to this function.  However, it goes further, and only allows
the \texttt{orbit} function to be called if there is a \texttt{GroupAction} relationship
between \texttt{g} and \texttt{a}.\\

While the typeclasses were able to specify the core of what it means to be a group, and what
it means to have a group action on a set, the type system has its limits.  Haskell is centered
around the type of functions, but cannot make guarantees about other properties of these functions,
such as associativity, identity, and other properties necessary to have a proper mathematical group.

While these properties of functions cannot easily be guaranteed by Haskell (or any language with a lesser type-system
such as C, C++, or Java), there exist a family of languages with \texttt{dependent types} in which
this may be possible. The programming languages Idris and Coq are popular dependantly typed programming
languages, with Idris being implemented in Haskell.
\\

Another potential drawback of the strict type-system that Haskell has is the absence of constant-time lookup
data structures.  As mentioned, permutations have a very natural group action, and are thus
a popular group representation in computational group theory.  This project, as well as many
popular computational group theory systems, store permutations as lookup tables.  This emphasized the problem
that Haskell's implementation of lookup tables has logarithmic, rather than constant, runtime.
In practice, the specific implementation of the lookup table in Haskell turns out to be sufficient,
but from the perspective of theoretical bounds, this is concerning.  Future work would do well do
implement permutations as native Haskell functions, since lookup and composition would be readily built
into the language.
\\

Haskell's functional focus did lend itself to the elegant expression of the basic algorithms of computational
group theory.  List comprehensions, readily understood by a mathematician familiar with set-builder notation,
are an elegant way to generate combinations of elements; an idea fundamental to the algorithms explored in
this paper.  Folding over these lists provides a natural way to (functionally) build up structures given
an initial starting point, as we saw in every algorithm considered in this paper.
\\

In all, Haskell is a programming language that has properties that make it appeal for computational
group theory and other general applications.
Given the strictness of Haskell, it may provide a barrier for entry that mathematicians
might not find worthwhile.
However, there are certainly type-system niceties and higher-order
idioms that a programmer can appreciate and take advantage of.
This is evidenced an experimental general mathematics library in Haskell called \emph{HaskellForMaths} \cite{HaskellForMaths}.
There is certainly potential to develop a
fully functional computational group theory system in Haskell.
