\section{Introduction to Group Theory}

To talk about computational algebra, and computational group theory in specific,
we need to first be familiar with the basics
of the group structure, and some of
the interesting uses and computations we can perform with it.
Fraleigh \cite{Fraleigh67} or any other abstract algebra or group theory book should cover
the material in this section.


\subsection{The Group Structure}

\begin{definition}
    A group $(G, \cdot)$ is a set
    $G$ together with an associative binary operation $\cdot : G \times G \to G$.

    $(G, \cdot)$ must satisfy the following properties:
    \begin{enumerate}
        \item Identity: There exists $e \in G$ such that for all
            $a \in G$, $a \cdot e = e \cdot a = a$.
        \item Inverses: For all $a \in G$, there exists a (unique) element, written $a^{-1}$
            such that $a \cdot a^{-1} = a^{-1} \cdot a = e$.
    \end{enumerate}
\end{definition}

\subsection{Group Action}

Group actions are a common application of group theory.  The idea is that
a group $(G, \cdot)$ acts on a set $X$.

\begin{definition}
    A group action is a function $. : G \times \Omega \to \Omega$, where $(G, \cdot)$
    is a group and $\Omega$ is a set. Specifically, we call $\Omega$ a $G-$set.
    Given $g \in G$ and $w \in \Omega$, we call $g.w$ the action of $g$ on $w$.

    This group action must satisfy a few properties:
    \begin{enumerate}
        \item Compatibility: For all $a,b \in G$ and all $x \in X$, $(a \cdot b).x = a.(b.x)$.
        \item Identity: For all $x \in X$, $e.x = x$
    \end{enumerate}
\end{definition}
\\

Consider the set of all symmetries of an equilateral triangle centered about
$(0,0)$ in the Euclidean plane.  These must be distance preserving maps from $\mathbb{R}^2$ to
$\mathbb{R}^2$ in which $(0,0)$ is mapped to itself.  Then, from geometry, we get that
the set of symmetries must be made up of either:
\begin{enumerate}
    \item
        Reflections that cross through the center point and one of the corners of the triangle
    \item
        Rotations by multiples of $120^{\circ}$ about the center point.
\end{enumerate}

Consider the following triangle:
\begin{center}
    \includegraphics[scale=0.5]{triangle.png}
\end{center}

We can uniquely define these symmetries by how they move the corners of the triangle:
\begin{multicols}{2}
\begin{enumerate}
    \item[$\rho_0$: ]
        The identity transformation.

        \begin{tabular}{c || c | c | c }
            $x$ & $1$ & $2$ & $3$\\
            \hline
            $\rho_0(x)$ & $1$ & $2$ & $3$\\
        \end{tabular}

    \item[$\rho_1$: ]
        The single rotation.

        \begin{tabular}{c || c | c | c }
            $x$ & $1$ & $2$ & $3$\\
            \hline
            $\rho_1(x)$ & $2$ & $3$ & $1$\\
        \end{tabular}

    \item[$\rho_2$: ]
        The double rotation.

        \begin{tabular}{c || c | c | c }
            $x$ & $1$ & $2$ & $3$\\
            \hline
            $\rho_2(x)$ & $3$ & $1$ & $3$\\
        \end{tabular}

    \item[$\tau_1$: ]
        The reflection about the line through the center and point 1.

        \begin{tabular}{c || c | c | c }
            $x$ & $1$ & $2$ & $3$\\
            \hline
            $\tau_1(x)$ & $1$ & $3$ & $2$\\
        \end{tabular}

    \item[$\tau_2$: ]
        The reflection about the line through the center and point 2.

        \begin{tabular}{c || c | c | c }
            $x$ & $1$ & $2$ & $3$\\
            \hline
            $\tau_2(x)$ & $3$ & $2$ & $1$\\
        \end{tabular}

    \item[$\tau_3$: ]
        The reflection about the line through the center and point 3.

        \begin{tabular}{c || c | c | c }
            $x$ & $1$ & $2$ & $3$\\
            \hline
            $\tau_3(x)$ & $2$ & $1$ & $3$\\
        \end{tabular}

\end{enumerate}
\end{multicols}

Observe that the permutations given above do in fact form a group under function composition.
$\rho_0$ is the identity, as it does not move any points.  Each $\tau_i$ is its own inverse, and
$\rho_1$ and $\rho_2$ are inverses for each other.

We can now look at the group action of these permutations on
the set of corners of the equilateral triangle, with the natural group action being
function application. Observe that $\rho_1.1 = 2$, that is
the group action of $\rho_1$ on 1 yields 2.

\subsection{Orbits and Stabilizers}

Now that we are familiar with groups and group actions, we can talk about the structure
that arises from the group action.  A natural question to ask is, ``If I have an element from
the set, what other elements can I get if I use the entire group and group action relationship?''
We call this the orbit.

\begin{definition}
    Given a group $(G, \cdot)$ acting on a set $\Omega$, the \emph{orbit} of $w \in \Omega$ is
    defined as the set $orb_G(w) = \{ g.w \vert g \in G \}$.
\end{definition}

%We simply take every element from the group, let it act on $w$, and keep the result.
%The name ``orbit'' makes sense because we consider all the places $w$ got moved to under
%the $G$-actions.
%\\

Next we have the stabilizer.  In some sense, the orbit and stabilizer are opposite structures.
Given a group $(G, \cdot)$ with a $G-$set $\Omega$, the stabilizer for $w \in \Omega$ is the
set of all $g \in G$ that fix $w$ under the group action.

\begin{definition}
    Given a group $(G, \cdot)$ acting on a set $\Omega$, the \emph{stabilizer} of $w \in \Omega$
    is defined  as the set
    $stab_G(x) = \{ g \in G \vert g.x = x \}$.
\end{definition}

We look at every element in the group, and keep only the ones that do nothing to $w$.
The name ``stabilizer'' comes from the fact that every element in the stabilizer of $w$ will
not move $w$.

Stabilizers are of particular interest due to the fact that they are sub-groups of $(G,\cdot)$.
Furthermore, we have the \emph{Orbit-Stabilizer Theorem}, which relates the cardinality of $orb_G(w)$, $G$,
and $stab_G(w)$.
