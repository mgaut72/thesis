all:
	pdflatex -interaction=nonstopmode thesis.tex;\
	cp thesis.pdf GautreauM.pdf

clean:
	rm -f *.log *.aux *.pdf *.dvi
